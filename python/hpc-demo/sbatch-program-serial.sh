#!/bin/bash
#SBATCH --job-name=program-serial
#SBATCH --output=output-%x-%j.log

BASE_DIR="${HOME}/demo-code/python/hpc-demo"
DATA_DIR="${BASE_DIR}/data"
RESULT_DIR="${BASE_DIR}/results"

PYTHON="python"
SCRIPT="${BASE_DIR}/program-serial.py"


module is-loaded devel/python || module load devel/python
srun --label $PYTHON "$SCRIPT" -d "$DATA_DIR" -o "$RESULT_DIR"

