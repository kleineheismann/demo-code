#!/bin/bash
### config ###
PROC_NAME=$(basename "$0")

DEFAULT_OUTPUT_LOG_DIR="output"
DEFAULT_COUNT=1

### functions ###
function print_help() {
	cat <<E-O-H
Usage: $PROC_NAME [OPTIONS] CMD [CMD_ARGS ...]

Options:
-n COUNT    number of parallel jobs
            (Default: $DEFAULT_COUNT)
-o DIR      directory for the job output logfiles
            (Default: $DEFAULT_OUTPUT_LOG_DIR)
E-O-H
}

### argv ###
OUTPUT_LOG_DIR=""
COUNT=""

while test $# -gt 0 ; do
  case "$1" in
  -h|--help)
    print_help
    exit 0
    ;;
  -n)
    COUNT="$2"
    shift
    shift
    ;;
  -o)
    OUTPUT_LOG_DIR="$2"
    shift
    shift
    ;;
  --)
    shift
    break
    ;;
  *)
    break
    ;;
  esac
done

if test "$COUNT" == "" ; then
  COUNT="$DEFAULT_COUNT"
fi

if ! test "$COUNT" -gt 0 2>/dev/null ; then
	echo "COUNT must be a positive integer" >&2
	exit 64
fi

if test "$OUTPUT_LOG_DIR" == "" ; then
  OUTPUT_LOG_DIR="$DEFAULT_OUTPUT_LOG_DIR"
fi

if test $# -le 0 ; then
  echo "Missing parameter CMD" >&2
  exit 64
fi

### action ###
if ! test -d "$OUTPUT_LOG_DIR" ; then
  mkdir -p "$OUTPUT_LOG_DIR"
fi

PIDS=""
for i in $(seq 1 "$COUNT") ; do
	output_file="${OUTPUT_LOG_DIR}/local-job.$$.${i}.out"
	echo "Starting local job $i (Output in $output_file)"
	echo "# $*" > "$output_file"
	"$@" >> "$output_file" 2>&1 &
	PIDS="$PIDS $!"
done

echo "Waiting for local jobs to finish (process ids: $PIDS)"
wait

