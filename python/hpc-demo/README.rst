ABOUT
=====
This repository contain some code,
to demonstrate how to use the BWUniCluster HPC systems.


DEMOS
=====

Preparations
------------
::

$ module load devel/python
$ python generate-datasets.py -n 1000
$ ls data
$ ls data | wc -l


Serial Programm
---------------
::

$ python program-serial.py
$ ls results
$ ls data | wc -l
$ time python program-serial.py


Parallelisation via BASH jobs
-----------------------------
::

$ time ./start-local-jobs.sh -n 40 python program-serial.py
$ ls results | wc -l


Parallelisation via SLURM tasks
-------------------------------
::

$ srun      -p multiple     --nodes 25 --ntasks-per-node 40 --test-only python program-serial.py
$ srun      -p multiple     --nodes 25 --ntasks 958         --test-only python program-serial.py
$ srun      -p dev_multiple --nodes 4  --ntasks 158         --test-only python program-serial.py
$ time srun -p dev_multiple --nodes 4  --ntasks 158                     python program-serial.py
$ ls results | wc -l
$ sbatch    -p dev_multiple --nodes 4  --ntasks 160                     sbatch-program-serial.sh
$ squeue
$ watch squeue
$ ls results | wc -l


Parallelisation via MPI
-----------------------
::

$ module load mpi/openmpi
$ pip install -r requirements.txt
$ mpirun -n 40 python program-mpi.py -n 160
$ sbatch -p dev_multiple --nodes 4 --ntasks 160 sbatch-program-mpi.sh


AUTHOR
======
Jens Kleineheismann <kleineheismann@kit.edu>


LICENSE
=======
Permission to use, copy, modify, and/or distribute this software
for any purpose with or without fee is hereby granted.
