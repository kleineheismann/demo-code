#!/usr/bin/env python
import argparse
import glob
import json
import os
import random
import sys
import time
from mpi4py import MPI

PROCESS_DELAY = 5


class DataSet:
    def __init__(self):
        self.id = '1'
        self.values = []


class MPIMessage:
    OK = 1
    ERROR = 2
    DATASET = 3
    RESULT = 4

    def __init__(self, code=None, dataset=None, result=None):
        if code is None:
            code = self.OK
        self.code = code
        self.dataset = dataset
        self.result = result


class Program:
    _default_amount = 1
    _default_data_dir = './data'
    _default_result_dir = './results'
    _default_dataset_file_suffix = '.dat'

    def _setup_argparser(self, parser):
        parser.add_argument('-n', '--amount',
                            dest='amount', metavar='NUMBER',
                            type=int,
                            default=self._default_amount)
        parser.add_argument('-d', '--data_dir',
                            dest='data_dir', metavar='DIR',
                            default=self._default_data_dir)
        parser.add_argument('-o', '--output_dir',
                            dest='output_dir', metavar='DIR',
                            default=self._default_result_dir)
        return parser

    def _parse_args(self, argv=None):
        if argv is None:
            argv = sys.argv[1:]
        if not argv:
            argv = ()

        return self._argparser.parse_args(argv)

    @staticmethod
    def _seed_rng():
        random.seed(None)
        init = random.randint(0, 9)
        init += os.getpid()
        random.seed(init)

    def _log(self, msg, error=False):
        text = ''
        if self._rank == self._master_rank:
            text += 'Master: '
        else:
            text += 'Worker {}: '.format(self._rank)
        text += msg
        if error:
            sys.stderr.write(text + '\n')
        else:
            sys.stdout.write(text + '\n')

    def __init__(self):
        self._argparser = argparse.ArgumentParser()
        self._setup_argparser(self._argparser)
        self._seed_rng()

        self._dataset_file_suffix = self._default_dataset_file_suffix

        self._comm = MPI.COMM_WORLD
        self._rank = self._comm.Get_rank()
        self._master_rank = 0

    def __call__(self, *args, **kwargs):
        argv = kwargs.get('argv', None)
        cmd_args = self._parse_args(argv)

        if self._comm.Get_size() < 2:
            self._log('Number of MPI processes must be greater 1', error=True)
            exitval = os.EX_USAGE
        elif self._rank == self._master_rank:
            exitval = self.master(cmd_args.data_dir, cmd_args.output_dir, amount=cmd_args.amount)
        else:
            exitval = self.worker()

        return exitval

    def read_dataset(self, input_path, attempts=100):
        glob_pattern = os.path.join(input_path, '*{suffix}'.format(suffix=self._dataset_file_suffix))
        my_pid = os.getpid()
        my_lock_content = '{}\n'.format(my_pid)

        n = 0
        while n < attempts:
            n += 1
            # Try to choose a dataset file (randomly)
            try:
                # Choose a dataset file (randomly).
                # Will raise IndexError, if there is nothing to choose from.
                filepath = random.choice(glob.glob(glob_pattern))
            except IndexError:
                raise FileNotFoundError('No dataset files available')

            # Try to mark the choosen dataset file as locked
            lock_filepath = filepath + '.lock'
            try:
                # Create a lock file.
                # Will raise a FileExistsError, if it already exists.
                with open(lock_filepath, 'x') as f_lock:
                    # Have create a lock file. Mark it as mine.
                    f_lock.write(my_lock_content)
                # Check if the dataset file was not processed/removed before I could lock it.
                # This is possible, because choosing and locking is not atomic.
                if os.path.exists(filepath):
                    # Managed to lock a dataset file before anybody else processed/removed it.
                    # Break the loop and process the dataset file.
                    break
                # Dataset file is already gone. I was to slow.
                # Remove my lock again and try another round/attempt/file.
                with open(lock_filepath, 'r') as f_lock:
                    lock_content = f_lock.read()
                if lock_content != my_lock_content:
                    self._log('Warning: Exclusive creation of lock files seems not to be supported')
                else:
                    os.unlink(lock_filepath)
            except FileExistsError:
                # The lock file already exists, which means someone else have
                # choosen this dataset file.
                # Try anouther round/attempt/file
                continue
        else:
            raise TimeoutError('Cannot get exclusive access to a dataset file')

        # To prevent anybody else to choose my dataset file, I will rename it.
        tmp_filepath = '{filepath}.{pid}'.format(filepath=filepath, pid=my_pid)
        os.rename(filepath, tmp_filepath)

        basename = os.path.basename(filepath)
        ds_id, _ = os.path.splitext(basename)
        with open(tmp_filepath, 'r') as f:
            ds_values = json.load(f)

        self._log('Read dataset file {}'.format(filepath))

        os.unlink(tmp_filepath)
        os.unlink(lock_filepath)

        dataset = DataSet()
        dataset.id = ds_id
        dataset.values = ds_values
        return dataset

    def process_dataset(self, dataset):
        self._log('Process dataset {}'.format(dataset.id))
        time.sleep(PROCESS_DELAY)
        result = dataset.values[-1]
        return result

    def write_result(self, dataset, result, output_path):
        basename = '{id}{suffix}'.format(id=dataset.id,
                                         suffix=self._dataset_file_suffix)
        filepath = os.path.join(output_path, basename)

        try:
            os.makedirs(output_path)
        except FileExistsError:
            pass

        with open(filepath, 'w') as f:
            json.dump(result, f)
            f.write('\n')

        self._log('Wrote result file {}'.format(filepath))

    def master(self, data_dir, output_dir, amount=1):
        comm = self._comm
        exitval = os.EX_OK

        n_send = 0
        n_recv = 0
        n_err = 0
        while n_recv < amount or amount == -1:
            msg_status = MPI.Status()
            msg = comm.recv(source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG, status=msg_status)
            msg_partner = msg_status.Get_source()

            if msg.code == MPIMessage.OK:
                self._log('Received OK from worker {}'.format(msg_partner))
                if n_send < amount or amount == -1:
                    try:
                        dataset = self.read_dataset(data_dir)
                    except FileNotFoundError as e:
                        if amount != -1:
                            self._log(str(e), error=True)
                            exitval = os.EX_NOINPUT
                        else:
                            self._log(str(e))
                        amount = n_send
                        continue
                    except TimeoutError as e:
                        self._log(str(e), error=True)
                        exitval = os.EX_TEMPFAIL
                        break

                    msg = MPIMessage(code=MPIMessage.DATASET, dataset=dataset)
                    self._log('Sending dataset {} to worker {}'.format(dataset.id, msg_partner))
                    comm.send(msg, dest=msg_partner)
                    n_send += 1
                else:
                    self._log('Already send {} datasets'.format(n_send))
            elif msg.code == MPIMessage.RESULT:
                self._log('Got result {} from worker {}'.format(msg.dataset.id, msg_partner))
                self.write_result(msg.dataset, msg.result, output_dir)
                n_recv += 1
            else:
                self._log('Error: got {} from worker {}'.format(msg.code, msg_partner), error=True)
                n_err += 1

            self._log('Recv: {}, Send: {}, Errors: {}'.format(n_recv, n_send, n_err))

        self._log('Finished -> Sending OK to all workers')
        requests = []
        for i in range(1, comm.Get_size()):
            req = comm.isend(MPIMessage(), dest=i)
            requests.append(req)
        req.waitall(requests)
        return exitval

    def worker(self):
        comm = self._comm
        master_rank = self._master_rank
        exitval = os.EX_OK

        n_recv = 0
        n_send = 0
        n_proc = 0
        n_err = 0
        while True:
            msg = MPIMessage()
            self._log('Sending OK to master')
            comm.send(msg, dest=master_rank)

            msg = comm.recv(source=master_rank)
            if msg.code == MPIMessage.DATASET:
                self._log('Got dataset {} from master'.format(msg.dataset.id))
                n_recv += 1
                result = self.process_dataset(msg.dataset)
                n_proc += 1
                msg = MPIMessage(code=MPIMessage.RESULT, dataset=msg.dataset, result=result)
                self._log('Sending result to master')
            elif msg.code == MPIMessage.OK:
                self._log('Got OK from master')
                break
            else:
                self._log('Error: got {} from master'.format(msg.code), error=True)
                n_err += 1
                msg = MPIMessage(code=MPIMessage.ERROR)
                self._log('Sending ERROR to master')
            comm.send(msg, dest=master_rank)
            n_send += 1

        self._log('Recv: {}, Send: {}, Proc: {}, Errors: {}'.format(n_recv, n_send, n_proc, n_err))
        return exitval


def main(*args, **kwargs):
    program = Program()
    exitval = program(*args, **kwargs)
    sys.exit(exitval)


if __name__ == '__main__':
    main()
