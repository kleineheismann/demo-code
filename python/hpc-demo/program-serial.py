#!/usr/bin/env python
import argparse
import glob
import json
import os
import random
import sys
import time

PROCESS_DELAY = 5


class DataSet:
    def __init__(self):
        self.id = '1'
        self.values = []


class Program:
    _default_amount = 1
    _default_data_dir = './data'
    _default_result_dir = './results'
    _default_dataset_file_suffix = '.dat'

    def _setup_argparser(self, parser):
        parser.add_argument('-n', '--amount',
                            dest='amount', metavar='NUMBER',
                            type=int,
                            default=self._default_amount)
        parser.add_argument('-d', '--data_dir',
                            dest='data_dir', metavar='DIR',
                            default=self._default_data_dir)
        parser.add_argument('-o', '--output_dir',
                            dest='output_dir', metavar='DIR',
                            default=self._default_result_dir)
        return parser

    def _parse_args(self, argv=None):
        if argv is None:
            argv = sys.argv[1:]
        if not argv:
            argv = ()

        return self._argparser.parse_args(argv)

    @staticmethod
    def _seed_rng():
        random.seed(None)
        init = random.randint(0, 9)
        init += os.getpid()
        random.seed(init)

    @staticmethod
    def _log(msg, error=False):
        text = ''
        text += 'Process {}: '.format(os.getpid())
        text += msg
        if error:
            sys.stderr.write(text + '\n')
        else:
            sys.stdout.write(text + '\n')

    def __init__(self):
        self._argparser = argparse.ArgumentParser()
        self._setup_argparser(self._argparser)
        self._seed_rng()

        self._dataset_file_suffix = self._default_dataset_file_suffix

    def __call__(self, *args, **kwargs):
        argv = kwargs.get('argv', None)
        cmd_args = self._parse_args(argv)
        exitval = self.main(cmd_args.data_dir, cmd_args.output_dir, amount=cmd_args.amount)
        return exitval

    def read_dataset(self, input_path, attempts=100):
        glob_pattern = os.path.join(input_path, '*{suffix}'.format(suffix=self._dataset_file_suffix))
        my_pid = os.getpid()
        my_lock_content = '{}\n'.format(my_pid)

        n = 0
        while n < attempts:
            n += 1
            # Try to choose a dataset file (randomly)
            try:
                # Choose a dataset file (randomly).
                # Will raise IndexError, if there is nothing to choose from.
                filepath = random.choice(glob.glob(glob_pattern))
            except IndexError:
                raise FileNotFoundError('No dataset files available')

            # Try to mark the choosen dataset file as locked
            lock_filepath = filepath + '.lock'
            try:
                # Create a lock file.
                # Will raise a FileExistsError, if it already exists.
                with open(lock_filepath, 'x') as f_lock:
                    # Have create a lock file. Mark it as mine.
                    f_lock.write(my_lock_content)
                # Check if the dataset file was not processed/removed before I could lock it.
                # This is possible, because choosing and locking is not atomic.
                if os.path.exists(filepath):
                    # Managed to lock a dataset file before anybody else processed/removed it.
                    # Break the loop and process the dataset file.
                    break
                # Dataset file is already gone. I was to slow.
                # Remove my lock again and try another round/attempt/file.
                with open(lock_filepath, 'r') as f_lock:
                    lock_content = f_lock.read()
                if lock_content != my_lock_content:
                    self._log('Warning: Exclusive creation of lock files seems not to be supported')
                else:
                    os.unlink(lock_filepath)
            except FileExistsError:
                # The lock file already exists, which means someone else have
                # choosen this dataset file.
                # Try anouther round/attempt/file
                continue
        else:
            raise TimeoutError('Cannot get exclusive access to a dataset file')

        # To prevent anybody else to choose my dataset file, I will rename it.
        tmp_filepath = '{filepath}.{pid}'.format(filepath=filepath, pid=my_pid)
        os.rename(filepath, tmp_filepath)

        basename = os.path.basename(filepath)
        ds_id, _ = os.path.splitext(basename)
        with open(tmp_filepath, 'r') as f:
            ds_values = json.load(f)

        self._log('Read dataset file {}'.format(filepath))

        os.unlink(tmp_filepath)
        os.unlink(lock_filepath)

        dataset = DataSet()
        dataset.id = ds_id
        dataset.values = ds_values
        return dataset

    def process_dataset(self, dataset):
        self._log('Process dataset {}'.format(dataset.id))
        time.sleep(PROCESS_DELAY)
        result = dataset.values[-1]
        return result

    def write_result(self, dataset, result, output_path):
        basename = '{id}{suffix}'.format(id=dataset.id,
                                         suffix=self._dataset_file_suffix)
        filepath = os.path.join(output_path, basename)

        try:
            os.makedirs(output_path)
        except FileExistsError:
            pass

        with open(filepath, 'w') as f:
            json.dump(result, f)
            f.write('\n')

        self._log('Wrote result file {}'.format(filepath))

    def main(self, data_dir, output_dir, amount=1):
        exitval = os.EX_OK
        n = 0
        while n < amount or amount == -1:
            try:
                dataset = self.read_dataset(data_dir)
            except FileNotFoundError as e:
                if amount != -1:
                    self._log(str(e), error=True)
                    exitval = os.EX_NOINPUT
                break
            result = self.process_dataset(dataset)
            self.write_result(dataset, result, output_dir)
            n += 1

        self._log('Processed {} dataset files'.format(n))
        return exitval


def main(*args, **kwargs):
    program = Program()
    exitval = program(*args, **kwargs)
    sys.exit(exitval)


if __name__ == '__main__':
    main()
