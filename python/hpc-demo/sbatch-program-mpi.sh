#!/bin/bash
#SBATCH --job-name=program-mpi
#SBATCH --output=output-%x-%j.log

BASE_DIR="${HOME}/demo-code/python/hpc-demo"
DATA_DIR="${BASE_DIR}/data"
RESULT_DIR="${BASE_DIR}/results"

PYTHON="python"
SCRIPT="${BASE_DIR}/program-mpi.py"


module is-loaded devel/python || module load devel/python
module is-loaded mpi/openmpi || module load mpi/openmpi
mpirun $PYTHON "$SCRIPT" -d "$DATA_DIR" -o "$RESULT_DIR" -n -1

