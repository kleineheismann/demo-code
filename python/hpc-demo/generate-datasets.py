#!/usr/bin/env python
import argparse
import json
import os
import random
import sys


class DataSet:
    def __init__(self):
        self.id = '1'
        self.values = []


class Program:
    _default_amount = 10
    _default_data_dir = './data'
    _default_dataset_file_suffix = '.dat'

    def _setup_argparser(self, parser):
        parser.add_argument('-n', '--amount',
                            dest='amount', metavar='NUMBER',
                            type=int,
                            default=self._default_amount)
        parser.add_argument('-d', '--data_dir',
                            dest='data_dir', metavar='DIR',
                            default=self._default_data_dir)
        return parser

    def _parse_args(self, argv=None):
        if argv is None:
            argv = sys.argv[1:]
        if not argv:
            argv = ()

        return self._argparser.parse_args(argv)

    @staticmethod
    def _seed_rng():
        random.seed(None)
        init = random.randint(0, 9)
        init += os.getpid()
        random.seed(init)

    @staticmethod
    def _log(msg, error=False):
        if error:
            sys.stderr.write(msg + '\n')
        else:
            sys.stdout.write(msg + '\n')

    def __init__(self):
        self._argparser = argparse.ArgumentParser()
        self._setup_argparser(self._argparser)
        self._seed_rng()

        self._dataset_file_suffix = self._default_dataset_file_suffix

    def __call__(self, *args, **kwargs):
        argv = kwargs.get('argv', None)
        cmd_args = self._parse_args(argv)
        exitval = self.main(cmd_args.amount, cmd_args.data_dir)
        return exitval

    @staticmethod
    def get_random_value():
        return random.randint(0, 100)

    def produce_dataset(self, previous=None):
        dataset = DataSet()
        if previous is not None:
            dataset.id = str(int(previous.id) + 1)
        dataset.values = (int(dataset.id),
                          self.get_random_value(),
                          self.get_random_value(),
                          self.get_random_value())
        return dataset

    def write_dataset(self, dataset, output_path):
        basename = '{id}{suffix}'.format(id=dataset.id,
                                         suffix=self._dataset_file_suffix)
        filepath = os.path.join(output_path, basename)

        try:
            os.makedirs(output_path)
        except FileExistsError:
            pass

        with open(filepath, 'w') as f:
            json.dump(dataset.values, f)
            f.write('\n')

        self._log('Created dataset file {}'.format(filepath))

    def main(self, amount, output_path):
        dataset = None
        i = 0
        while i < amount:
            dataset = self.produce_dataset(dataset)
            self.write_dataset(dataset, output_path)
            i += 1
        
        self._log('Created {} dataset files'.format(i))
        return os.EX_OK


def main(*args, **kwargs):
    program = Program()
    exitval = program(*args, **kwargs)
    sys.exit(exitval)


if __name__ == '__main__':
    main()
