###
### Functions for the work releated stuff 
### (create work packages, process a single work package,
###  show a single result, ...)
###
create_work_packages<-function(n, size=100, random=FALSE) {
	r = vector("list", n)
	for(i in 1:n) {
		if(random)
			data = sample(1:99, size, replace=TRUE)
		else
			data = 1:size
		wp = list(id=i, data=data)
		r[[i]] = wp
	}
	return(r)
}

work<-function(work_package, max_size=NULL, delay=0.0, debug=FALSE) {
	if(is.null(max_size))
		max_size = length(work_package$data)

	t_start = Sys.time()
	# product does not mean the mathematical product,
	# but the result of any work though.
	product = sum(work_package$data[1:max_size])
	Sys.sleep(delay)
	t_end = Sys.time()

	r = list(
		id = work_package$id,
		product = product,
		t_start = t_start,
		t_end = t_end,
		worker_profile = identify_worker()
	)
	if(debug)
		print_result(r)
	return(r)
}

identify_worker<-function() {
	proc_self_stat<-read.delim("/proc/self/stat", header=FALSE, sep=" ")

	r = list(
		nodename = as.character(Sys.info()["nodename"]),
		mpi_rank = mpi.comm.rank(0),
		pid = Sys.getpid(),
		cpu_id = proc_self_stat$V39
	)

	return(r)
}

print_result<-function(result) {
	t_start_str = format(result$t_start, "%H:%M:%OS6")
	t_end_str = format(result$t_end, "%H:%M:%OS6")

	cat("Work: ", result$id, ", Product: ", result$product, "\n", sep="")
	cat(" Time: ", t_start_str, " - ", t_end_str, "\n", sep="")
	w = result$worker_profile
	cat(" Node: ", w$nodename, ", pid: ", w$pid,
		", cpu: ", w$cpu_id, "\n", sep="")
	cat(" MPI Rank: ", w$mpi_rank, "\n", sep="")
	cat("\n")
}

