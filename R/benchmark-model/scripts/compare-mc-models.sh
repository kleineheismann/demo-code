#!/bin/sh
ID="compare_mc_models"
PACKAGES="20 100"
SIZES="10 1000"
DELAYS="0 0.5"

MODELS="sequential MC parFORK futureMS futureMC futureCallr"

for model in $MODELS ; do
	for p in $PACKAGES ; do
		for s in $SIZES ; do
			for d in $DELAYS ; do
				./benchmark-model.R -m "$model" -p "$p" -s "$s" -d "$d" \
					-R "results/${ID}-${model}-${p}-${s}-${d}.rds"
			done
		done
	done
done

# If there is a previous overall result file -> remove it.
test -e "results/${ID}.rds" && rm "results/${ID}.rds"

# Merge in the single result files.
for model in $MODELS ; do
	for p in $PACKAGES ; do
		for s in $SIZES ; do
			for d in $DELAYS ; do
				./merge-dataframes.R -o "results/${ID}.rds" \
					-i "results/${ID}-${model}-${p}-${s}-${d}.rds"
				rm "results/${ID}-${model}-${p}-${s}-${d}.rds"
			done
		done
	done
done

