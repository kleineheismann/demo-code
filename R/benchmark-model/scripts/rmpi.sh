#!/bin/sh

mpirun -n 3 ./benchmark-model.R -P -r 1 -d 1 -m MPI -p 12
mpirun -n 3 ./benchmark-model.R -P -r 1 -d 1 -m MPI_MC -w 2 -p 12

