#!/bin/sh 
########## Begin Slurm header ##########
#
# Give job a reasonable name
#SBATCH --job-name=rmpi_test
#
#SBATCH --partition dev_multiple
#
# Request number of nodes for job
#SBATCH --nodes=3
#
# Number of program instances to be executed
#SBATCH --tasks-per-node=3
#
# Maximum run time of job
#SBATCH --time=00:10:00
#
# Write standard output and errors in same file
#SBATCH --output="slurm/output-%x-%j.log"
#
# Send mail when job begins, aborts and ends
#SBATCH --mail-type=BEGIN,END,FAIL
#
############ End Slurm header ##########

echo "Working Directory: $PWD"
echo "Running on host    `hostname`"
echo "Number of nodes:   $SLURM_NNODES"
echo "Number of tasks:   $SLURM_NTASKS"

# Setup R and Rmpi Environment
module load math/R/4.1.2
module load mpi/openmpi/4.1

mpirun ./benchmark-model.R -C configs/rmpi_test.yml

